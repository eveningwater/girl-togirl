import styled from '@emotion/styled';
import Router from './router/router';
import Configer from './store/store';

const StyleApp = styled.div({
    background:`#eee`,
    padding:'16px 16px 50px',
    minHeight:'100vh',
    backgroundSize:"100% 100%",
    width:"100%"
});

const StyleContent = styled.div`
    max-width:480px;
    margin: 0 auto;
`

const App = () => {
   const { Provider } = Configer;
   return (
     <StyleApp>
        <Provider>
            <StyleContent>
                <Router />
            </StyleContent>
        </Provider>
     </StyleApp>
   )
}

export default App
