import React, { cloneElement, ReactElement,forwardRef, MutableRefObject } from "react";
import "./carousel.css";
import useCarousel,{ CarouselInstance,useChildrenHandler,ImperItem, ImperRef } from "./useCarousel";
export interface CarouselProps {
  options?: { src: string; alt: string,text?: string }[];
  children?: JSX.Element[];
  currentActive?: number;
  inst?: (v: CarouselInstance | null) => void;
}
export interface CarouselItemProps {
  src?: string;
  text?: string;
  alt?: string;
  children?: JSX.Element[];
  className?: string;
}
export interface CustomComponent extends React.ForwardRefExoticComponent<CarouselProps & React.RefAttributes<ImperRef>> {
  CarouselItem: typeof CarouselItem;
}
export const CarouselItem = (props: CarouselItemProps) => {
  const { src, text,alt = "图片加载中", className, children, ...rest } = props;
  return (
    <>
      {children ? (
        children
      ) : (
        <div className={["carousel-item", className].join(" ")} {...rest}>
           {
             src && (<img src={src} alt={alt} className="carousel-item-img" />)
           }
           {
             text && (<div className="carousel-item-text">{text}</div>)
           }
        </div>
      )}
    </>
  );
};
CarouselItem.displayName = "CarouselItem";
const Carousel = forwardRef((props: CarouselProps,ref:ImperItem['ref']) => {
  const { children, options, currentActive = 0, ...rest } = props;
  const boxId = Math.floor(Math.random() * 10000);
  const items =
    Array.isArray(options) && options.length
      ? options
      : children?.filter(
          (item) => item?.type?.displayName === "CarouselItem"
        ) || [];
  const instance = useCarousel({
    el: `.carousel-box-${boxId}`,
    speed: 1000, // 轮播速度(ms)
    delay: 0, // 轮播延迟(ms)
    direction: 'left', // 图片滑动方向
    monitorKeyEvent: true, // 是否监听键盘事件
    monitorTouchEvent: true // 是否监听屏幕滑动事件
  });
  useChildrenHandler(ref as MutableRefObject<ImperRef>, {
     onStart: () => instance?.start(),
     onPause: () => instance?.pause()
  });
  return (
    <div className={["carousel-box",`carousel-box-${boxId}`].join(' ')}>
      <div className="carousel-content">
        {items?.map((item, index: number) => {
          const { src,alt,text = '' } = typeof (item as unknown as ReactElement).type === 'undefined' ? item : (item as unknown as ReactElement).props;
          return cloneElement(
            <CarouselItem
              key={`item-${index}`}
              className={currentActive === index ? "active" : ""}
            />,
            {
              ...rest,
              src,
              alt,
              text
            }
          );
        })}
      </div>
      <div className="carousel-sign">
        {items?.map((item, index: number) => (
          <div
            className={[
              "carousel-sign-item",
              currentActive === index ? "active" : "",
            ].join(" ")}
            key={`item-sign-${index}`}
          >
            {index}
          </div>
        ))}
      </div>
      <div className="carousel-ctrl carousel-left-ctrl">&lt;</div>
      <div className="carousel-ctrl carousel-right-ctrl">&gt;</div>
    </div>
  );
}) as CustomComponent;
Carousel.CarouselItem = CarouselItem;
export default Carousel;
