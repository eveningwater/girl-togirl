import "../style/drag.css";

const Drag = () => {
    const getStyle = () => ({
        maxWidth: window.innerWidth - 200 + 10, 
        maxHeight: window.innerHeight - 180 + 10,
    })
    return (
        <div className="layout">
            <div className="container">
                <div className="resize" style={{ ...getStyle() }}></div>
                <div className="content">Lorem ipsum dolor sit amet consectetur?</div>
            </div>
        </div>
    )
}

export default Drag;