import useAnyDrag from "./hooks/useAnyDrag";
import './App.css';
import useIsScroll from "./hooks/useIsScroll";
import { createRef, useRef } from "react";
// import Drag from './views/Drag';
import Carousel from "./components/Carousel";
import { ImperRef } from "./components/useCarousel";
const App = () => {
   const { x, y, isMove, centerX, minX, maxX } = useAnyDrag(() => document.querySelector('#drag'));
   const {isScroll} = useIsScroll();
   const scrollElement = createRef<HTMLDivElement>()
   const getLeftPosition = () => {
      if (!x || !centerX || isMove) {
         return x;
      }
      if (x <= centerX) {
         return minX || 0;
      } else {
         return maxX;
      }
   }
   const scrollPosition = () => {
      if (typeof getLeftPosition() === 'number') {
         // 150是元素宽度
         if (getLeftPosition() === 0) {
            return -((scrollElement.current?.offsetWidth || 150) / 2);
         } else {
            return getLeftPosition() as number + (scrollElement.current?.offsetWidth || 150) / 2;
         }
      }
      return 0;
   }
   const imgArrs = [
       {
         src:"https://www.eveningwater.com/img/segmentfault/1.png",
         alt:"图片1"
       },
       {
         src:"https://www.eveningwater.com/img/segmentfault/2.png",
         alt:"图片2"
       },
       {
         src:"https://www.eveningwater.com/img/segmentfault/3.png",
         alt:"图片3"
       },
       {
         src:"https://www.eveningwater.com/img/segmentfault/4.png",
         alt:"图片4"
       },
       {
         src:"https://www.eveningwater.com/img/segmentfault/5.png",
         alt:"图片5"
       }
   ]
   const carouselRef = useRef<ImperRef>(null)
   const onPauseHandler = () => {
      carouselRef.current?.onPause();
   }
   const onStartHandler = () => {
      carouselRef.current?.onStart();
   }
   return (
      <div className="App">
         {/* <div className="overHeight"></div>
         <div className={`${ isScroll ? 'drag transition' : 'drag'}`}
            style={{ left: (isScroll ? scrollPosition() : getLeftPosition()) + 'px', top: y + 'px' }}
            id="drag"
            ref={scrollElement}
         ></div> */}
         {/* <Drag /> */}
         <button onClick={onPauseHandler} style={{ position:'fixed',left:0,top:0 }}>点击我暂停</button>
         <button onClick={onStartHandler} style={{ position:'fixed',left:100,top:0 }}>点击我开始</button>
         <div style={{ width:500,height: 400,margin:'auto'}}>
            <Carousel options={imgArrs} ref={carouselRef}/>
         </div>
         <div style={{ width:500,height: 400,margin:'15px auto'}}>
            <Carousel>
                {
                  imgArrs.map(((item) => (
                     <Carousel.CarouselItem {...item} key={`item-${item.src}`}></Carousel.CarouselItem>
                  )))
                }
            </Carousel>
         </div>
      </div>
   )
}
export default App;