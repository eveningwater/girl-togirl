import useAnyDrag from "./hooks/useAnyDrag";
import './App.css';
import useIsScroll from "./hooks/useIsScroll";
import { createRef } from "react";
// import Drag from './views/Drag';

const App = () => {
   const { x, y, isMove, centerX, minX, maxX } = useAnyDrag(() => document.querySelector('#drag'));
   const isScroll = useIsScroll();
   const scrollElement = createRef<HTMLDivElement>()
   const getLeftPosition = () => {
      if (!x || !centerX || isMove) {
         return x;
      }
      if (x <= centerX) {
         return minX || 0;
      } else {
         return maxX;
      }
   }
   const scrollPosition = () => {
      if (typeof getLeftPosition() === 'number') {
         // 150是元素宽度
         if (getLeftPosition() === 0) {
            return -((scrollElement.current?.offsetWidth || 150) / 2);
         } else {
            return getLeftPosition() as number + (scrollElement.current?.offsetWidth || 150) / 2;
         }
      }
      return 0;
   }
   return (
      <div className="App">
         <div className="overHeight"></div>
         <div className='drag'
            style={{ left: (isScroll ? scrollPosition() : getLeftPosition()) + 'px', top: y + 'px' }}
            id="drag"
            ref={scrollElement}
         ></div>
         {/* <Drag /> */}
      </div>
   )
}
export default App;